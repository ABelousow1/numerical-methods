#pragma once
#include <vector>
#include <functional>
#include "../Numerics1/vecs.h"
using namespace std;
class sysdifeq : public vector<function<double(double, vec)>> {
public:
	vec eval(double t, vec u);
};