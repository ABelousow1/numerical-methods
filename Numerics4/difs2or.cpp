#include "difs2or.h"

void solveLinEquation2Order(LinEquation2order eq, borderConditions2 bc, vec& u, int n) {
	LinTridiogonalEqs s(n, eq, bc);
	thomasSolve(s, u);
}

LinTridiogonalEqs::LinTridiogonalEqs(int n, LinEquation2order eq, borderConditions2 bc) :
	N(n), h((bc.x1 - bc.x0) / N)
{
	double& alpha1 = bc.alpha1,
		& alpha2 = bc.alpha2,
		& alpha3 = bc.alpha3,
		& beta1 = bc.beta1,
		& beta2 = bc.beta2,
		& beta3 = bc.beta3,
		& x0 = bc.x0,
		& x1 = bc.x1, & step = h;
	b.resize(2);
	for (auto &i : b) {
		i.resize(N - 2);
	}
	a1 = -alpha1 + alpha2 * h;
	b1 = alpha1;
	d1 = alpha3 * h;
	bn = -beta1;
	cn = beta1 + beta2 * h;
	dn = beta3 * h;
	double hsq = h * h;
	auto& p = eq.p;
	auto& q = eq.q;
	generate(b[0].begin(), b[0].end(),
		[x0, x1, hsq, step, p]() {
			static double z = x0; z += step; return -2 - p(z) * hsq; });
	generate(b[1].begin(), b[1].end(),
		[x0, x1, hsq, step, q]() {
			static double z = x0; z += step; return q(z)*hsq; });

}

tuple<double, double, double, double> LinTridiogonalEqs::getLine(int i)
{
	if (i == 0) {
		return { a1, b1, 0, d1 };
	}
	else
		if (i == N - 1) {
			return { 0, bn, cn, dn };
		}
		else {
			return { 1, b[0][i-1], 1, b[1][i-1] };
		}
}
