#include <iostream>
#include <functional>
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#define _USE_MATH_DEFINES
#include <cmath>
using namespace std;

int main() {
	double x0 = 0, x1 = 3.1415926538, y0 = 0, y1 = 3.141592653589793,
		t1 = 100;
	int Nx = 5, Ny = 5, Nt = 1000;
	double
		hx = (x1 - x0) / Nx, hy = (y1 - y0) / Ny, ht = t1 / Nt;
	double D = 1;
	function<double(double, double)>border_x0 = [](double y, double t) {return sin(y*t); };
	function<double(double, double)>border_x1 = [](double y, double t) {return 1; };
	function<double(double, double)>border_y0 = [](double x, double t) {return 0; };
	function<double(double, double)>border_y1 = [](double x, double t) {return exp(-x*t-1); };
	function<double(double, double)>start_t0  = [](double x, double y) {return 0; };
	
	vector<vector<vector<double>>> u;
	u.resize(Nt+1);
	for (auto& i : u)
	{
		i.resize(Nx+1);
		for (auto& j : i) {
			j.resize(Ny+1);
		}
	}
	for (double i = 0, x = 0; i <= Nx; ++i, x+=hx) {
		for (double j = 0, y = 0; j <= Ny; ++j, y+=hy) {
			u[0][i][j] = start_t0(x, y);
		}
	}
	for (double i = 0, t = 0; i <= Nt; ++i, t += ht) {
		for (double j = 0, y = 0; j <= Ny; ++j, y += hy) {
			u[i][0][j] = border_x0(y, t);
		}
		auto end = u[0].size()-1;
		for (double j = 0, y = 0; j <= Ny; ++j, y += hy) {
			u[i][end][j] = border_x1(y, t);
		}
		end = u[0][0].size()-1;
		for (double j = 0, x = 0; j <= Nx; ++j, x += hx) {
			u[i][j][0] = border_y0(x, t);
		}
		for (double j = 0, x = 0; j <= Nx; ++j, x += hx) {
			u[i][j][end] = border_y1(x, t);
		}
	}

	double hx2 = hx * hx;
	double hy2 = hy * hy;

	for (int i = 1; i <= 1; ++i) {
		for (int j = 1; j < Nx; ++j) {
			for (int k = 1; k < Ny; ++k) {
				u[i][j][k] = D*((u[i-1][j-1][k] - 2*u[i-1][j][k] + u[i-1][j+1][k])/hx2+
								(u[i-1][j][k-1] - 2* u[i-1][j][k] + u[i-1][j][k+1])/hy2)*ht
								+ u[i - 1][j][k];
			}
		}
	}
	for (int i = 2; i <= Nt; ++i) {
		for (int j = 1; j < Nx; ++j) {
			for (int k = 1; k < Ny; ++k) {
				u[i][j][k] = D*((u[i-1][j-1][k] - 2*u[i-1][j][k] + u[i-1][j+1][k])/hx2+
								(u[i-1][j][k-1] - 2* u[i-1][j][k] + u[i-1][j][k+1])/hy2)*ht*2
								+ u[i - 2][j][k];
			}
		}
	}

	ofstream fl("script.js");
	string str;
	stringstream ss;
	ss << "document.querySelector('#rng').addE; var ux = [";
	for (double x = x0; x <= x1;ss << x << ", ", x += hx);
	ss << "];\nvar uy =[";
	for (double y = y0; y <= y1; ss << y << ", ", y += hy);
	ss << "]\nu = [";
	for (auto &i : u) {
		ss << "[";
		for (auto& j : i) {
			ss << "[";
			for (auto& k : j) {
				ss << k << ", ";
			}
			ss << "],\n";
		}
		ss << "],\n";
	}
	ss << "];\n";
	ss << "document.querySelector('#rng').addEventListener('input', function(event){\n"
		" var s = (u.length-1)/(document.querySelector('#rng').max-document.querySelector('#rng').min);\n"
		"var val = Math.floor(event.target.value * s);\n"
		"var data = [{z: u[val], x : ux, y : uy, type : 'heatmap'}];\n"
		"Plotly.newPlot('myDiv', data); });";
	fl << ss.str();


	return 0;
}