#include "difs2or.h";
#include <cmath>

using std::vector;


int main() {
	//double a1=0, a2=1, x1 = (3.141592653589793), a3=4, b1=0, b2=1, b3=-2 + 2*exp(x1), x0 = 0;
	//int N = 400;
	//borderConditions2 border{a1,a2,a3,b1,b2,b3,x0,x1};
	//LinEquation2order lineq{
	//	[](double x) {
	//		return -1;
	//	},[](double x) {
	//		return 4*exp(x);
	//	}
	//};
	//vec y;
	//solveLinEquation2Order(lineq, border, y, N);
	//double a = x0, h = (x1-x0)/(N-1);
	//for (int i = 0; i < N; ++i) {
	//	cout << a << " ";
	//	a += h;
	//}
	//cout << endl;
	//srand(0);
	///*for (double i = 0; i < x1; i+=h) {
	//	cout << 2 * cos(i) - 5 * sin(i) + 2 * exp(i) + 2 *((double)rand() / INT_MAX) - 1 << " ";
	//}*/
	//for (auto i : y) {
	//	cout << i << " ";
	//}
	int N = 10;
	double a = 0, b = 1, c1=1, c2=1, c3 = 2, d1=1, d2=1, d3=2;
	function<double(double)> q = [](double d) {return d * d + 1; };
	function<double(double)> f = [](double d) {return exp(-d*d); };

	vector<double> A(N+1);

	vector<vector<double>> C(N+1);
	for (auto& i : C) {
		i.resize(N + 2);
	}
	C[0][0] = c2;
	C[0][N+1] = c3;
	double abuf = 1;
	for (int i = 1; i <= N; ++i) {
		C[0][i] = c1 * i * abuf;
		abuf *= a;
		C[0][i] += c2 * abuf;
	}
	double bbuf = 1;
	C[N][0] = d2;
	C[N][N+1] = d3;
	for (int i = 1; i <= N; ++i) {
		C[N][i] = d1 * i * bbuf;
		abuf *= b;
		C[N][i] += d2 * bbuf;
	}
	double dx = (b - a)/N;
	double point = a + dx;
	for (int i = 1; i < N; ++i) {
		double pn = q(point), fn = f(point);
		double pointbuf = 1;
		C[i][0] = -pn;
		C[i][1] = -pn*point;
		for (int j = 2; j <= N; ++j) {
			C[i][j] = j * (j - 1) * pointbuf;
			pointbuf *= point;
			C[i][j] -= pn * pointbuf* point;
		}
		C[i][N+1] = fn;
		point += dx;
	}
	for (auto i : C) {
		for (auto j : i) {
			cout << j << " ";
		}
		cout << endl;
	}
	cout << endl;
	cout << endl;

	for (int i = 0; i <= N; ++i) {
		int max_i = 0;
		for (int j = i+1; j <= N; ++j) {
			max_i = (abs(C[max_i][i]) < abs(C[j][i])) ? j : max_i;
		}
		swap(C[max_i], C[i]);
		for (auto &i : C) {
			for (auto &j : i) {
				cout << j << " ";
			}
			cout << endl;
		}
		cout << endl;
		for (int j = 0; j <= N+1; ++j) {
			C[i][j] /= C[i][i];
		}
		for (auto i : C) {
			for (auto j : i) {
				cout << j << " ";
			}
			cout << endl;
		}
		cout << endl;
		for (int j = i + 1; j <= N; ++j) {
			for (int k = 0; k <= N+1; ++k) {
				C[j][k] -= C[j][i] * C[i][k];
			}
		}
		for (auto i : C) {
			for (auto j : i) {
				cout << j << " ";
			}
			cout << endl;
		}
		cout << endl;
		for (int j = 0; j < i; ++j) {
			for (int k = 0; k <= N+1; ++k) {
				C[j][k] -= C[j][i] * C[i][k];
			}
		}

		for (auto i : C) {
			for (auto j : i) {
				cout << j << " ";
			}
			cout << endl;
		}
		cout << endl;
		cout << endl;
	}
	for (auto i : C) {
		for (auto j : i) {
			cout << j << " ";
		}
		cout << endl;
	}
	cout << endl;
	cout << endl;
	return 0;
}