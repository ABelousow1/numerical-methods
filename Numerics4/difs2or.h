#pragma once

#include <functional>
#include "../Numerics1/vecs.h"
#include <algorithm>
using namespace std;

struct LinEquation2order {
	function<double(double)> q;
	function<double(double)> p;
};
struct borderConditions2 {
	double alpha1, alpha2, alpha3,
		beta1, beta2, beta3, x0, x1;
};
struct LinTridiogonalEqs : ITridiagonalLinearSystem {
	std::vector<vec> b;
	double a1, b1, d1, bn, cn, dn;

	int N;
	double h;
	LinTridiogonalEqs(int n, LinEquation2order eq, borderConditions2 bc);
	tuple<double, double, double, double> getLine(int i);
	size_t getSize() {
		return N;
	}
};
void solveLinEquation2Order(LinEquation2order eq, borderConditions2 bc, vec& u, int n = 20);