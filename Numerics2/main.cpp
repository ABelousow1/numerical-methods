#include <fstream>
#include "vectoreval.h"
#include <iomanip>
using namespace std;

int main() {
	double a = 0, b = 1, t = 0.05;
	int N = (int)((b - a) / t);
	
	sysdifeq U;
	U.push_back(([](double t, vec u) { return 2 * u[0] + u[1]; }));
	U.push_back(([](double t, vec u) { return 3 * u[0] + 4 * u[1]; }));

	vec begin;
	begin.push_back(2);
	begin.push_back(2);



	vector<vec> result;
	vec points;
	vector<vec> k(4);
	do {
		result.push_back(begin);
		points.push_back(a);
		k[0] = U.eval(a, begin);
		k[1] = U.eval(a + t / 4, begin + t / 4 * k[0] );
		k[2] = U.eval(a + t / 2, begin + t / 2 * k[1] );
		k[3] = U.eval(a + t, begin + t * (k[0] - 2 * (k[1] - k[2])));
		auto s = (k[0] + 4 * k[2] + k[3]);
		auto sum = t*s/6;
		begin = begin + sum;
		a = a + t;
	
	} while (a <= b);

	for (auto i : points) {
		cout << setw(9) << i << '\t';
	}
	cout << endl;
	for (int j = 0; j < result[0].size(); ++j) {
		for (int i = 0; i < result.size(); ++i) {
			cout << setw(9) << result[i][j] << '\t';
		}
		cout << endl;
	}
	
	return 0;
}