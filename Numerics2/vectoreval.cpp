#include "vectoreval.h"
#include <algorithm>

vec sysdifeq::eval(double t, vec u)
{
	vec res(this->size()) ;
	auto iterRes = res.begin();
	auto iterArgs = u.begin();
	transform(this->begin(), this->end(), res.begin(), 
		[t, u](function<double (double, vec)> &f) 
		{
			return f(t, u);
		});
	return res;
}
