#include "vecs.h"
#include <algorithm>
#include <fstream>

using namespace std;

using eigen_pair = pair<double, vec>;
using eigen_pairs = vector<eigen_pair>;
eigen_pair quotient_Realays(matr A, double eps) {
	auto g = []() {return static_cast<double>(rand()%100000)/100; };
	vec y(A[0].size()); generate(y.begin(), y.end(), g);
	normalized(y);
	vec y1 = A * y;
	normalized(y1);
	double lambda = 0;
	double lambda1 = ((A * y) * y) / (y * y);
	do {
		lambda = lambda1;
		y = y1;
		y1 = A * y;
		double dot1 = y1 * y, dot = y * y;
		normalized(y);
		normalized(y1);
		lambda1 = dot1 / dot;
	}
	while (abs(lambda1 - lambda) > eps);
	
	return pair<double, vec>(lambda, y1);
}
eigen_pairs find_allEigenPairs(matr A, double eps) {
	int n = A.size();
	eigen_pairs res;
	for (int i = 0; i < n; ++i) {
		auto eigenPair = quotient_Realays(A, eps / (10 * (n - i)));
		res.push_back(eigenPair);
		A = A - eigenPair.first*mulvecM(eigenPair.second, eigenPair.second);

	}
	return res;
}
int main() {
	ifstream file("matr.txt");
	setlocale(LC_ALL, "Russian");
	int n;
	while (file >> n) {
		matr A(n, n);
		file >> A;
		auto res = find_allEigenPairs(A, 0.00001);
		for (auto i : res) {
			cout << "(" << i.first << "; " << i.second << ")\n";
			auto B = A - idmatr(i.first);
			cout << "проверка!!!! " << B * i.second << endl;
		}
	}
	return 0;
}
///1 2 3 2 2 4 3 4 3