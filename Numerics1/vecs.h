#pragma once
#include <iostream>
#include <vector>
#include <iterator>
#include <tuple>
using namespace std;
class vec : public vector<double, allocator<double>> {
	bool trans = false;
public:
	vec() :vector<double>() {}
	//vec(vec &v): vector<double>(v){}
	//vec(vec v): vector<double>(v){}
	vec(int n) : vector<double>(n) {}
	//vec(vec& v): vector<double>(v), trans(v.trans) {}
};
double operator*(const vec& v1, const vec& v2);
vec operator*(double a, const vec& v);
vec operator+(const vec v1, const vec v2);
vec operator/(const vec& v, const double& a);
vec& operator-=(vec& v1, const vec& v2);
vec operator-(const vec v1, const vec v2)	  ;
vec normalize(vec& v)						  ;
void operator*=(vec& v, const double& a)	  ;
istream& operator>>(istream& c, vec &v)		  ;
ostream& operator << (ostream& c, vec v)	  ;

//using matr = vector<vec>;
class matr : public vector<vec> {
public:
	matr(int n, int m) : vector<vec>(n) {
		for (auto& i : *this) {
			i = vec(m);
		}
	}
	matr() : vector<vec>() {}
};
matr operator*(const double a, const matr& A) ;
vec operator*(const matr& A, const vec& v)	  ;
void operator/=(vec& v, const double& a);
vec normalize(vec& v);
void normalized(vec& v);
double abs(vec v);

istream& operator>>(istream& c, matr& m);
ostream& operator<<(ostream& c, matr& m);

class idmatr : matr {
public:
	static matr fullidentity(size_t n) {
		matr res;
		res.resize(n);
		for (int j = 0; auto & i : res)
			i.resize(n), ++j, i[j] = 1;
		return res;
	}
	double ed = 1;
	idmatr(double d) {
		ed = d;
	}
};

matr operator-(matr &m, idmatr &id);
matr operator-(matr m, idmatr id);
matr operator-(matr &m1, matr &m2);
matr operator-(matr m1, matr m2);
matr mulvecM(vec v1, vec v2);

matr inverse(matr& m);

class ITridiagonalLinearSystem {

public:
	virtual tuple<double, double, double, double> getLine(int i) = 0;
	virtual size_t getSize() = 0;
};
void thomasSolve(ITridiagonalLinearSystem &s, vec& x);