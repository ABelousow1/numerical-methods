#include "vecs.h"
using namespace std;

//using vec = vector<double>;

double operator*(const vec& v1, const vec& v2) {
	double res = 0;
	for (int i = 0; i < v1.size(); ++i) {
		res+= v1[i]*v2[i];
	}
	return res;
}
vec operator*(double a, const vec& v) {
	vec res(v);
	for (int i = 0; i < v.size(); ++i) {
		res[i] *= a;
	}
	return res;
}
vec operator+(const vec v1, const vec v2) {
	vec res(v1);
	for (int i = 0; i < v1.size(); ++i) {
		res[i] += v2[i];
	}
	return res;
}
vec operator/(const vec& v, const double& a) {
	return (1 / a) * v;
}
vec& operator-=(vec& v1, const vec& v2)
{
	for (int i = 0, s = v1.size(); i < s; ++i) {
		v1[i] -= v2[i];
	}
	return v1;
}
vec operator-(const vec v1, const vec v2) {
	return v1 + (-1) * v2;
}
void operator*=(vec& v, const double& a) {
	for (int i = 0; i < v.size(); ++i) {
		v[i] *= a;
	}
}
void operator/=(vec& v, const double& a) {
	v *= (1 / a);
}
vec normalize(vec& v) {
	vec res(v);
	return v / sqrt(v * v);
}
void normalized(vec& v) {
	v /= sqrt(v * v);
}
double abs(vec v) {
	return sqrt(v * v);
}
istream& operator>>(istream& c, matr& m)
{
	for (auto& i : m) {
		c >> i;
	}
	return c;
}
ostream& operator<<(ostream& c, matr& m)
{
	for (auto& i : m) {
		c << i;
		c << endl;
	}
	return c;
}
matr operator-(matr &m, idmatr &id)
{
	int n = m.size();
	matr res(m);
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			res[i][i] -= id.ed;
		}
	}
	return res;
}
matr operator-(matr m, idmatr id)
{
	auto n = m.size();
	for (int i = 0; i < n; ++i){
			m[i][i] -= id.ed;
	}
	return m;
}
matr operator-(matr& m1, matr& m2)
{
	auto res(m1);
	int n = m1.size(), m = m1[0].size();
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j) {
			res[i][j] = m1[i][j] - m2[i][j];
		}
	}
	return res;
}
matr operator-(matr m1, matr m2)
{
	auto res(m1);
	int n = m1.size(), m = m1[0].size();
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j) {
			res[i][j] = m1[i][j] - m2[i][j];
		}
	}
	return res;
}
matr mulvecM(vec v1, vec v2)
{
	int n = v1.size(), m = v2.size();
	matr res(n, m);
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j) {
			res[i][j] = v1[i] * v2[j];
		}
	}
	return res;
}
matr inverse(matr& m)
{
	matr id(m);
	matr res = idmatr::fullidentity(m.size());
	for (int i = 0, s = res.size(); i < s; ++i) {
		//id[i] /= id[i][i];
		res[i] /= id[i][i];
		for (int j = i + 1; j < s; ++j) {
			//id[j] -= (id[j][i] * id[i]);
			res[j] -= (id[j][i] * id[i]);
		}
	}
	return res;
}
void thomasSolve(ITridiagonalLinearSystem &s, vec& x)
{
	//forward
	auto N = s.getSize();
	vec alpha(N), beta(N-1);
	if (N == 0) return;
	tuple<double, double, double, double> line = s.getLine(0);
	double a, b, c, d;
	a = std::get<0>(line);
	b = std::get<1>(line);
	d = std::get<3>(line);
	alpha[0] = -b/a;
	beta[0] =  d/a;

	for (int i = 1; i < N-1; ++i) {
		line = s.getLine(i);
		a = std::get<0>(line);
		b = std::get<1>(line);
		c = std::get<2>(line);
		d = std::get<3>(line);
		double div = a*alpha[i - 1] + b;
		alpha[i] = -c / div;
		beta[i] = (d - a * beta[i - 1]) / div;
	}
	//backward
	line = s.getLine(N-1);
	b = std::get<1>(line);
	c = std::get<2>(line);
	d = std::get<3>(line);
	x.resize(N);
	x[N - 1] = (d - beta[N - 2] * b) / (b * alpha[N - 2] + c);
	for (int i = N - 2; i >= 0; --i) {
		x[i] = alpha[i] * x[i + 1] + beta[i];
	}
	return;
}
istream& operator>>(istream& c, vec &v) {
	int n = v.size();
	for (int i = 0; i < n; ++i) {
		c >> v[i];
	}
	return c;
}
ostream& operator << (ostream& c, vec v) {
	for (auto& i : v) {
		c << i << " ";
	}
	return c;
}



matr operator*(const double a, const matr& A) {
	matr res(A);
	for (auto &i : res) {
		i *= a;
	}
	return res;
}
vec operator*(const matr& A, const vec& v) {
	vec res(v);
	for (int i = 0; i < v.size(); ++i) {
		res[i] = 0; 
		for (int j = 0; j < v.size(); ++j) {
			res[i] += A[i][j] * v[j];
		}
	}
	return res;
}

